﻿DROP DATABASE IF EXISTS ejemploTrigger;
CREATE DATABASE ejemploTrigger;
USE ejemploTrigger;

CREATE OR REPLACE TABLE local(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(50),
  precio float,
  plazas int
  );

CREATE OR REPLACE TABLE usuario(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(50),
  inicial char(1)
  );

CREATE OR REPLACE TABLE usa(
  local int,
  usuario int,
  dias int,
  fecha date,
  total float,
  PRIMARY KEY(local,usuario)
  );


/**
quiero que cuando introduzca un usuario me calcule y almacene la inicial de su nombre
**/

  DELIMITER //
  CREATE OR REPLACE TRIGGER usuarioBI
  BEFORE INSERT
    ON usuario
    FOR EACH ROW
  BEGIN
    SET NEW.inicial=LEFT(new.nombre,1);
  END //
  DELIMITER ;

 /*realiza parecido al disparador pero sobre toda la tabla*/
UPDATE usuario u SET u.inicial=LEFT(nombre,1);


TRUNCATE usuario;
INSERT INTO usuario (nombre)
  VALUES ('Roberto'),('ana'),('kevin');
SELECT * FROM usuario u;

/** modificamos la tabla usuarios */

ALTER TABLE usuario
  ADD COLUMN contador int DEFAULT 0;

/**
  inserto un usuario en usa me debe sumar a contador un 1
  **/


  DELIMITER //
  CREATE OR REPLACE TRIGGER usaAI
  AFTER INSERT
    ON usa
    FOR EACH ROW
  BEGIN
      UPDATE usuario
        SET contador=contador+1
        WHERE id=new.usuario;
      /*UPDATE usuario u 
        SET contador=(SELECT COUNT(*) FROM usa WHERE usuario=new.usuario)
        WHERE id=new.usuario;*/
  END //
  DELIMITER ;

TRUNCATE usa;
INSERT INTO usa (local, usuario)
  VALUES (1, 1),(1,2),(2,1);
SELECT * FROM usuario;
SELECT * FROM usa;

/**
  Consulta de actualizacion que realice lo que hace el disparador 
  anterior.
  Me tiene que colocar en contador las veces que ese usuario
  sale en lam tabla usa

/**
  no puedo
**/

    /*UPDATE usuario u JOIN usa ON usa.usuario=u.id
      GROUP BY usa.usuario
      SET contador=COUNT(*);*/

/* c1 */
SELECT usuario,COUNT(*) FROM usuario u JOIN usa ON usa.usuario=u.id
      GROUP BY usa.usuario;

/**
  optimizacion de C1
**/
SELECT usuario,COUNT(*) FROM usa GROUP BY usuario;


UPDATE usuario u JOIN (
  SELECT usuario,COUNT(*) numero FROM usa GROUP BY usuario
  ) c1
  ON c1.usuario=u.id
  SET contador=numero;

UPDATE usuario u 
  SET contador=(
  SELECT COUNT(*) FROM usa WHERE usuario=u.id
  );

SELECT * FROM usuario u;
SELECT * FROM usa;


/**
  calcular el total de usa en funcion del precio 
  del local cuando introducimos un dato nuevo
  en usa
  */

  DELIMITER //
  CREATE OR REPLACE TRIGGER usaBI
  BEFORE INSERT
    ON usa
    FOR EACH ROW
  BEGIN
    SET new.total=new.dias*(
      SELECT precio FROM local WHERE id=new.local
    );
  END //
  DELIMITER ;

TRUNCATE local;
INSERT INTO local (nombre, precio)
  VALUES ('l1', 10),('l2',5),('l3',7);
TRUNCATE usa;
INSERT INTO usa (local, usuario, dias)
  VALUES (1, 1, 5),(2,1,7),(1,3,80);

/**
  Consulta de actualizacion que me calcula el total
  de la tabla USA en funcion de los dias y el local,
  lo mismo que realiza el disparador anterior
**/
  UPDATE usa 
    SET total=dias*(
      SELECT precio FROM local WHERE id=local
    );
    


