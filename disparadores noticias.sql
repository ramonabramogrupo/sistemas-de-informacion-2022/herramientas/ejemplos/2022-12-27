﻿DROP DATABASE IF EXISTS disparadores;
CREATE DATABASE disparadores;
USE disparadores;

CREATE TABLE noticias(
id int AUTO_INCREMENT,
texto varchar(100),
titulo varchar(100),
longitud int,
fecha date,
dia int,
mes varchar(20),
PRIMARY KEY(ID)
);

INSERT INTO noticias (texto, titulo,fecha) VALUES 
  ('ejemplo de noticia larga', 'larga', '2022/12/1'),
  ('ejemplo de noticia corta', 'corta', '2022/1/12');


CREATE TABLE historico(
  id int AUTO_INCREMENT,
  dia int,
  numero int DEFAULT 0,
  PRIMARY KEY (id)
);




# DISPARADOR
# CALCULA LA LONGITUD DE CARACTERES DEL
# TEXTO DE UNA NOTICIA 
# CUANDO LA INSERTO 

DROP TRIGGER IF EXISTS binoticias;
DELIMITER //
CREATE TRIGGER binoticias
BEFORE INSERT
  ON noticias
  FOR EACH ROW
BEGIN
  SET NEW.longitud=CHAR_LENGTH(NEW.texto);
END //
DELIMITER ;


SELECT * FROM NOTICIAS;

INSERT INTO noticias (texto, titulo, fecha) VALUES 
  ('esta noticia es nueva', 'nueva', '2022/12/12');


# DISPARADOR
# Realizar un disparador para que cuando 
# creemos una noticia nueva nos calcule el mes 
# y el día de la fecha y los introduzca 
# en los campos mes y día

DROP TRIGGER IF EXISTS binoticias2;
DELIMITER //
CREATE TRIGGER binoticias2
BEFORE INSERT
  ON noticias
  FOR EACH ROW
BEGIN
  SET New.dia=DAY(NEW.fecha),New.mes=MONTHNAME(New.fecha);
END //
DELIMITER ;


INSERT INTO noticias (texto, titulo, fecha) VALUES 
  ('esta noticia es mas nueva', 'mas nueva', '2022/1/12');

# VOY A ACTUALIZAR UNA DE LAS NOTICIAS
UPDATE noticias n
  SET n.texto="algo diferente"
  WHERE n.id=4;

SELECT * FROM noticias n;

# OTRO DISPARADOR

DROP TRIGGER IF EXISTS bunoticias;
DELIMITER //
CREATE TRIGGER bunoticias
BEFORE UPDATE
  ON noticias
  FOR EACH ROW
BEGIN
  SET NEW.longitud=CHAR_LENGTH(NEW.texto),
         NEW.dia=DAY(NEW.fecha),
         NEW.mes=MONTHNAME(NEW.fecha);
END //
DELIMITER ;


# VOY A ACTUALIZAR UNA DE LAS NOTICIAS
UPDATE noticias n
  SET n.texto="algo diferente"
  WHERE n.id=4;

# tabla de historico

-- consulta de seleccion
SELECT 
    n.dia,
    COUNT(*) total 
  FROM 
    noticias n
  GROUP BY n.dia;

-- consulta de creacion

DROP TABLE IF EXISTS historico;

CREATE TABLE historico(
  id int AUTO_INCREMENT,
  dia int,
  numero int DEFAULT 0,
  PRIMARY KEY (id)
) AS 
    SELECT 
        n.dia,
        COUNT(*) numero 
      FROM 
        noticias n
      GROUP BY n.dia;

SELECT * FROM historico h;
DELETE FROM noticias WHERE ID=4;
SELECT * FROM noticias n;


# disparador de eliminacion
# Crear un DISPARADOR que me ACTUALICE la tabla HISTORICO 
# cada vez que ELIMINO una noticia de la tabla NOTICIAS

DROP TRIGGER IF EXISTS adnoticias;
DELIMITER //
CREATE TRIGGER adnoticias
AFTER DELETE
  ON noticias
  FOR EACH ROW
BEGIN
  UPDATE historico h
    SET h.numero=h.numero-1
    WHERE h.dia=OLD.dia;
END //
DELIMITER ;

# inserto registros nuevos en noticias
INSERT INTO noticias (texto, titulo, fecha)
  VALUES ('ejemplo noticias', 'ti1', '2022/5/8'),('ejemplo noticias 1', 'ti2', '2022/8/7');

# disparador
# Crear un DISPARADOR que me ACTUALICE la tabla HISTORICO 
# cada vez que INSERTO una noticia en la tabla NOTICIAS

DROP TRIGGER IF EXISTS ainoticias;
DELIMITER //
CREATE TRIGGER ainoticias
AFTER INSERT
  ON noticias
  FOR EACH ROW
BEGIN
  DECLARE contador int DEFAULT 0;

  SELECT 
      COUNT(*) INTO contador -- si contador=1 el dia ya esta en historico
    FROM 
      historico h 
    WHERE 
      h.dia=NEW.dia;
  
  IF (contador=1) THEN
      -- SI CONTADOR ES 1 INDICA QUE EN HISTORICO YA TENIA NOTICIAS DE ESE DIA
      -- SOLAMENTE TENGO QUE SUMAR 1
      UPDATE historico h
        SET h.numero=h.numero+1
        WHERE h.dia=NEW.dia;
  ELSE
      -- SI CONTADOR ES 0 INDICA QUE EN HISTORICO EL DIA NO EXISTE
      -- NO TENGO NOTICIAS DE ESE DIA
      -- TENGO QUE INSERTAR UN REGISTRO NUEVO
      INSERT INTO historico (dia, numero)
        VALUES (NEW.dia, 1); 
  END IF;

  
END //
DELIMITER ;

SELECT * FROM noticias;
SELECT * FROM historico h;

INSERT INTO noticias (texto, titulo, fecha)
  VALUES ('EJEMPLO NUEVA', 'nueva','2022/5/19'),('EJEMPLO NUEVA 1', 'nueva 2','2022/5/8')